<?php
/**
 * Testimonials widget
 *
 * Sept. 08, 2018
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('com/testimonials/lang/'.$service->get('Language')->getCode().'/testimonials');
$service->get('Ressource')->get('core/helper/actionmanager');
$service->get('Ressource')->get('core/widget/listwidget');
$service->get('Ressource')->get('core/display/thumbnail');
$service->get('Ressource')->get('core/display/html/htmlimage');
$service->get('Ressource')->get('core/display/html/htmllink');

class TestimonialsWidget extends ListWidget{
	private $wn;

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
        $w = 'testimonials';
        $this->wn = $w;
		$this->setInfo(array(
			'component' => $w,
			'type' => 'static',
			'name' => $w,
			'title' => TESTIMONIALS_WIDGET_TITLE,
			'description' => TESTIMONIALS_WIDGET_TITLE_DESC,
			'wireframe' => $w,
			'icon' => $w,
            'saveoptions' => $this->getOptionsNames(),
			'copyoptions' => [
				'data' => [
					'data' => [
						'select' => [
							'data_type' => '{parentname}item',
							'data_parent' => '{parentobjid}'
						],
						'update' => [
							'data_parent' => '{parentobjid}'
						]
					]
				]
			]
		));
	}

	public function render(){
		global $service;
		$opt = $this->data->getVar('widget_options');
        $i = $this->getInfo();
		$w = $this->data;
		$o = $w->getVar('widget_options');
		$service->get('Ressource')->getStyle('testimonials','widget','testimonials',$opt['stylesheet']);
        $content = '';
        return str_replace('{content}',$content,parent::render());
	}

	protected function editItem($data){
        global $service;
		$store = new DataStore();
		$defaultlang = $service->get('Language')->getDefault();
		$form = $data['form'];
		$form->setTitle(TESTIMONIALS_WIDGET_TITLE);
		$defobj = $store->getDefaultObj($data['data']);
		$obj = $data['data'];
        $form->add(new TextFormField('author',$defobj->getVar('data_data')['author'],array(
			'title'=>_AUTHOR,
			'width' => 3,
			'length'=>255,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'data_data','author')
		)));
        $form->add(new TextFormField('occupation',$defobj->getVar('data_data')['occupation'],array(
			'title'=>_OCCUPATION,
			'width' => 3,
			'length'=>255,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'data_data','occupation')
		)));
        $form->add(new TextFormField('company',$defobj->getVar('data_data')['company'],array(
			'title'=>_COMPANY,
			'width' => 3,
			'length'=>255,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'data_data','company')
		)));
        $form->add(new TextFormField('link',$defobj->getVar('data_data')['link'],array(
			'title'=>_LINK,
			'width' => 3,
			'length'=>255,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'data_data','link')
		)));
        $form->add(new HtmleditorFormField('text',$defobj->getVar('data_data')['text'],array(
			'title'=>_TEXT,
			'width' => 6,
			'length'=>255,
            'configfile' => 'config-admin',
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'data_data','text')
		)));
        $form->add(new ImageuploadFormField('image',$defobj->getVar('data_data')['image'],array(
			'title' => _PICTURE,
			'width' => 3
		)));
		$form->add(new YesnoFormField('displayitemtitle',$defobj->getVar('data_data')['displayitemtitle'],array(
			'title' => TESTIMONIALS_WIDGET_ITEM_DISPLAYTITLE,
			'width' => 3
		)));
		return $form;
	}

    protected function renderItem($v,$w) {
		$image = new HtmlImage();
        $thumb = new Thumbnail();
        $content = '<blockquote>';
        if ($v->getVar('data_data')['image']) {
            $content .= '<div class="picture">'.$image->attr('src',$v->getVar('data_data')['image'])->attr('alt',$v->getVar('data_title'))->open().'</div>';
        }
		if ($v->getVar('data_title')) {
            $content .= "<h1".((!$v->getVar('data_data')['displayitemtitle'])?' class="hidden"':'').">".$v->getVar('data_title')."</h1>";
        }
		if ($v->getVar('data_data')['text']) {
			$content .= '<div class="quote">'.$v->getVar('data_data')['text'].'</div>';
		}
        $content .= "<footer><cite>";
        if ($v->getVar('data_data')['link']) {
            $link = new HtmlLink();
    		$link->attr('href',$v->getVar('data_data')['link'])->attr('title',$v->getVar('data_title'));
            $content .= $link->open();
        }
		if ($v->getVar('data_data')['author']) {
			$content .= '<p class="author">'.$v->getVar('data_data')['author'].'</p>';
		}
		if ($v->getVar('data_data')['occupation']) {
			$content .= '<p class="occupation">'.$v->getVar('data_data')['occupation'].'</p>';
		}
		if ($v->getVar('data_data')['company']) {
			$content .= '<p class="company">'.$v->getVar('data_data')['company'].'</p>';
		}
        if ($v->getVar('data_data')['link']) {
            $content .= $link->close();
        }
        $content .= "</cite></footer>";
        $content .= '</blockquote>';
		return $content;
    }

	protected function saveItem() {
		return array('text','image','author','occupation','company','link','displayitemtitle');
	}
}
?>
